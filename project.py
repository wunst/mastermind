import random
import re
from tabulate import tabulate


# Define strings and constants
RIGHT_COL = "+"
RIGHT_NUM = "o"
WRONG_NUM = "-"
MIN_NUM = 1
MAX_NUM = 9
MAX_GUESS = 10
MSG_WELCOME = f"Welcome to Mastermind!\nWe will play a number-guessing game. You will choose the difficulty you want to play. The higher the difficulty the more columns to guess.\nI will randomly pick a number between {MIN_NUM} and {MAX_NUM} for each column without duplicates. Then you will guess the numbers I have picked.\nIf a number is in the solution and the right column, I will show a '{RIGHT_COL}'.\nIf a number is in the solution but the wrong column, I will show a '{RIGHT_NUM}'.\nIf a number is not included in the solution, I will show a '{WRONG_NUM}'.\nYou have {MAX_GUESS} tries to guess the solution. Have fun playing!\n"
MSG_PLAY = "\nWhich difficulty level do you want to play?\n1 = easy (3 columns), 2 = medium (4 columns), 3 = hard (5 columns). "
MSG_LEVEL = f" numbers between {MIN_NUM} and {MAX_NUM}: "


def main():
    print("---\n\n" + MSG_WELCOME + "\n")
    level = set_level()
    solution = create_solution(level)
    # Repeat user interaction until solution is successfully guessed and increase counter by one for each iteration.
    count = 1
    while check_guess(solution, level, count, get_input(level)):
        count += 1


# Get user input as int and add 2 to difficulty in order to get level (difficulty 1 -> 3 columns, difficulty 2 -> 4 columns, difficulty 3 -> 5 columns) and return as int
def set_level() -> int:
    '''
    Sets the difficulty for the current game.

    :raise EOFError: If user inputs CTRL + D
    :return: The level of the game as (difficulty + 2) columns
    :rtype: int
    '''
    while True:
        try:
            difficulty = int(input(MSG_PLAY).strip())
            if difficulty in range(1,4):
                return difficulty+2
            else:
#                raise ValueError       # for testing in test_project.py, to be commented for prod
                pass                    # to be uncommented for prod
        except ValueError:
#            raise ValueError           # for testing in test_project.py, to be commented for prod
            pass                        # to be uncommented for prod
        except EOFError:
            print()
            break


# Generate the solution as a random unique number between min and max num for each difficulty-based column and return solution as list of int
def create_solution(level) -> list:
    '''
    Generates the random solution to be guessed.

    :param level: Number of single digits to generate.
    :type level: int
    :raise ValueError: If level is not of type int and equal to 3, 4 or 5
    :return: A list of length equal to level containing unique integers between 1 and 9.
    :rtype: list
    '''
    if level in range(3,6):
        return random.sample(range(MIN_NUM, MAX_NUM+1), level)
    else:
        raise ValueError


# Get user input as a guess of numbers equivalent to difficulty level and return guess as list of int
def get_input(level) -> list:
    '''
    Captures user input to guess the solution.

    :param level: Number of single digits to guess.
    :type level: int
    :raise EOFError: If user inputs CTRL + D
    :return: A list of length equal to level containing user input as integers.
    :rtype: list
    '''
    while True:
        try:
            s = input("\n" + str(level) + MSG_LEVEL).strip()
            match = re.findall(r"\d", s)
            if len(match) == level:
                return list(map(int, match))
            else:
#                    raise ValueError       # for testing in test_project.py, to be commented for prod
                pass                    # to be uncommented for prod
        except EOFError:
            print()
            raise EOFError


# Compare guess (list of int) to solution (list of int) and generate feedback (list of str) based on game rules.
# Print feedback and guess in table format.
# Return False for correct solution in order to end while loop in main function.
def check_guess(solution, level, count, guess):
    '''
    Generates the random solution to be guessed.

    :param solution: List of unique integers.
    :type solution: list
    :param level: Number of digits to evaluate.
    :type level: int
    :param count: Counter of user guesses.
    :type count: int
    :param guess: List of integers.
    :type guess: list
    :return: True or False state based on successful guess or number of guesses.
    :rtype: boolean
    '''
    if count == 10:
        table = [solution, guess]
        print(f"\nGame over!\nYou took all {MAX_GUESS} tries. Here is the solution:\n" + tabulate(table, tablefmt="grid"))
        return False
    elif solution == guess:
        table = [solution, guess]
        print(f"\nSuccess!\nIt took you {count} tries to guess the solution.\n" + tabulate(table, tablefmt="grid"))
        return False
    else:
        feedback = []
        for i in range(level):
            if guess[i] == solution[i]:
                feedback.append(RIGHT_COL)
            elif guess[i] in solution:
                feedback.append(RIGHT_NUM)
            else:
                feedback.append(WRONG_NUM)
        table = [feedback, guess]
        print("\nTry again!\n" + tabulate(table, tablefmt="grid"))
        return True


if __name__ == "__main__":
    main()