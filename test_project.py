import pytest
from project import set_level, create_solution, get_input, check_guess


# Simulating user input with monkeypatch
def test_set_level(monkeypatch):
    monkeypatch.setattr('builtins.input', lambda _: "1")
    assert set_level() == 3
    monkeypatch.setattr('builtins.input', lambda _: "2")
    assert set_level() == 4
    monkeypatch.setattr('builtins.input', lambda _: "3")
    assert set_level() == 5
    with pytest.raises(ValueError):
        monkeypatch.setattr('builtins.input', lambda _: "0")
        set_level()
    with pytest.raises(ValueError):
        monkeypatch.setattr('builtins.input', lambda _: "4")
        set_level()
    with pytest.raises(ValueError):
        monkeypatch.setattr('builtins.input', lambda _: "E")
        set_level()


# Testing for length of solution as solution values are generated randomly
def test_create_solution():
    assert len(create_solution(3)) == 3
    assert all(isinstance(n, int) for n in create_solution(3)) == True
    assert len(create_solution(4)) == 4
    assert all(isinstance(n, int) for n in create_solution(4)) == True
    assert len(create_solution(5)) == 5
    assert all(isinstance(n, int) for n in create_solution(5)) == True
    with pytest.raises(ValueError):
        create_solution(2)
    with pytest.raises(ValueError):
        create_solution(6)


# Simulating user input with monkeypatch
def test_get_input(monkeypatch):
    monkeypatch.setattr('builtins.input', lambda _: "123")
    assert get_input(3) == [1, 2, 3]
    monkeypatch.setattr('builtins.input', lambda _: "1234")
    assert get_input(4) == [1, 2, 3, 4]
    monkeypatch.setattr('builtins.input', lambda _: "12345")
    assert get_input(5) == [1, 2, 3, 4, 5]
    with pytest.raises(ValueError):
        monkeypatch.setattr('builtins.input', lambda _: "12")
        get_input(3)
    with pytest.raises(ValueError):
        monkeypatch.setattr('builtins.input', lambda _: "1234")
        get_input(3)
    with pytest.raises(ValueError):
        monkeypatch.setattr('builtins.input', lambda _: "123")
        get_input(4)
    with pytest.raises(ValueError):
        monkeypatch.setattr('builtins.input', lambda _: "1234")
        get_input(5)
    with pytest.raises(ValueError):
        monkeypatch.setattr('builtins.input', lambda _: "1A!")
        get_input(3)


# In order for while loop to work, function must return False for correct results
def test_check_guess():
    assert check_guess([1, 2, 3], 3, 1, [1, 2, 3]) == False
    assert check_guess([1, 2, 3, 4], 4, 1, [1, 2, 3, 4]) == False
    assert check_guess([1, 2, 3, 4, 5], 5, 1, [1, 2, 3, 4, 5]) == False
    assert check_guess([1, 2, 3], 3, 10, [0, 2, 3]) == False
    assert check_guess([1, 2, 3], 3, 1, [0, 2, 3]) == True
    assert check_guess([1, 2, 3, 4], 4, 1, [0, 2, 3, 4]) == True
    assert check_guess([1, 2, 3, 4, 5], 5, 1, [0, 2, 3, 4, 5]) == True