# MASTERMIND GAME
#### Video Demo:  https://peertube.art3mis.de/w/jjmQ6jEtYbGuv1HZ6GFJiM
## Description:

This project builds the MASTERMIND game, a game I much enjoyed playing as a kid. You can read more about it in the [Mastermind Wikipedia Article](https://en.wikipedia.org/wiki/Mastermind_(board_game)). In a brief summary, the original game is for two players: the 'codemaker' generates a code from four coloured pegs, the 'codebreaker' tries to guess the code. After each guess, the codemaker provides feedback whether a peg has the right color and/or the right position in the code.

In this project, the computer takes the role of the codemaker and the user plays as codebreaker. Instead of coloured pegs the code consists of numbers. The feedback will be given in symbols where '+' is the right number at the right position, 'o' is the right number at the wrong position, and '-' is a wrong number. The game ends if the user has guessed the code correctly or after 10 unsuccessful tries.

### Playing the Game
At the beginning of each game, a short summary of the rules is printed. The user can choose the difficulty level between 1 and 3, where 1 will create 3 columns, 2 will create 4 columns, and 3 will create 5 columns. For each column, the codemaker will randomly generate a number between 1 and 9 without duplicates. The user (aka codebreaker) inputs a series of numbers to guess the code. After each try, the computer (aka codemaker) prints the feedback formatted as a table, with the first row as the hints and the second row as the user's input. This informs the codebreaker how to improve his guesses. After a successful guess or after 10 unsuccessful guesses, the table will show the solution in the first row and the user's last guess in the second row.

### Implementation
The project consists of the project.py file and the test_project.py file. In the additional requirements.txt I list the imported modules for both files.

In project.py I import the modules 'random' to generate the solution, 're' to validate user's input with a regular expression, and 'tabulate' to format the feedback output. I have defined constants for the game logic and text prompts at the top of the file in order to see them at a glance and be able to make them easily translatable in the future.

The *main* function prints the welcome message with a summary of the game's rules. Then it
- calls the function *level* to set the game's difficulty and stores it as an integer variable,
- calls the function *solution* to generate a random solution and stores it as a list variable,
- sets the count of codebreaker guesses to 1, and
- calls the function *check_guess* to evaluate the codebreaker's guess.

The key game logic happens in function *check_guess*, which takes *solution*, *level*, and *count* as arguments and itself calls the function *get_input* to get the codebreaker's guess. *check_guess* also prints the codemaker's feedback in a formatted way.

#### Function *set_level*
This function is pretty straight forward. It tries to store the user's input as an integer in variable *difficulty* and keeps repeating that until the input matches the requirement (type int of value 1, 2 or 3). It then adds 2 to the input and returns the result. This is necessary because the level (expressed in numbers of columns) equals the difficulty plus 2, i.e. difficulty 1 -> 3 columns, difficulty 2 -> 4 columns, difficulty 3 -> 5 columns.
```
[...]
difficulty = int(input(MSG_PLAY).strip())
if difficulty in range(1,4):
    return difficulty+2
[...]
```

#### Function *create_solution*
This function takes the previously captured difficulty level and generates one random number per column between constant minimum value (here: 1) and constant maximum value (here: 9). I struggled a bit to get this part right because I wanted to avoid duplicates in the solution (which in my opinion increase the difficulty significantly). I started with a for loop and checked if the generated number is already part of the solution and also tried the set() function. But these approaches meant that I won't get the full length in case of duplicates. I then researched and found the sample function of random module which is built to fulfil my requirement.
```
[...]
return random.sample(range(MIN_NUM, MAX_NUM+1), level)
[...]
```

#### Function *get_input*
This function is called by the function *check_guess* and returns its value as an argument for said function. This part was tricky to get right because I was determined to evaluate codebreaker's input with a regular expression. I wanted to give the users as much freedom as possible when entering their solutions, i.e. enter as a sequence of digits, separated with blanks or commas, or mix all of that. It was quickly apparent that I would not be successful with re.search and therefore switched to re.findall. Then I needed to convert the input from string to integers and store them in a list in order to make codebreaker's guess easily comparable to codemaker's solution.
```
[...]
s = input("\n" + str(level) + MSG_LEVEL).strip()
match = re.findall(r"\d", s)
if len(match) == level:
    return list(map(int, match))
[...]
```

#### Function *check_guess*
This function contains the key game logic. It takes as arguments codemaker's solution, the current game's difficulty level, the current guess count and the return value of *get_input* function. Then it goes through 3 evaluation steps (see below). If the solution is guessed correctly or all tries have been used, *check_guess* returns False and breaks the while loop it was called from in *main*. If the solution was not correctly guessed and there are tries left, it returns True and *main* calls it again, causing a new guess prompt for codebreaker.
- First I check if codebreaker has used their 10 attempts. If so, check_guess prints a respective message including the solution.
```
[...]
if count == 10:
    table = [solution, guess]
    print(f"\nGame over!\nYou took all {MAX_GUESS} tries. Here is the solution:\n" + tabulate(table, tablefmt="grid"))
[...]
```
- Second I check if codebreaker has correctly guessed the solution. If so, check_guess prints a respective message including the solution and the number of guesses.
```
[...]
elif solution == guess:
    table = [solution, guess]
    print(f"\nSuccess!\nIt took you {count} tries to guess the solution.\n" + tabulate(table, tablefmt="grid"))
[...]
```
- Third I check which part of codebreaker's guess matches the solution:
    - If the right number is in the right column, codemaker feedbacks '+'
    - If the right number is in the wrong column, codemaker feedbacks 'o'
    - If the number is not included in the solution, codemaker feedbacks '-'
```
[...]
feedback = []
        for i in range(level):
            if guess[i] == solution[i]:
                feedback.append(RIGHT_COL)
            elif guess[i] in solution:
                feedback.append(RIGHT_NUM)
            else:
                feedback.append(WRONG_NUM)
        table = [feedback, guess]
        print("\nTry again!\n" + tabulate(table, tablefmt="grid"))
[...]
```

### Unit Test
In test_project.py I import pytest in order to catch ValueErrors and to use monkeypatch to simulate user input. From project.py I import all functions except *main*.

The biggest challenge for me and definitely the most time consuming research task in this project was to find out how to simulate user input in a function. I wanted to keep input() as part of the respective function in project.py, and therefore I had to test the functions in test_project.py without passing an argument. I needed to create the user input programmatically while running the unit test. I experimented with monkeypatch and Mock() and other functions I found online until eventually this setup worked as intended. I've learned a lot during this problem solving process.

**In order for test_project.py to work, please read below carefully to understand which lines need to be commented and which uncommented in project.py before running the unit test.**

In the first test function *test_set_level* I simulate user input, check whether *set_level* returns the respective level (i.e. difficulty plus 2 as int) and catches the ValueError if codebreaker's input is not 1, 2 or 3 of type int. **In order to raise these ValueErrors, for testing I need to uncomment the respective lines of code in *set_level* and comment the 'pass' lines.**
```
[...]
monkeypatch.setattr('builtins.input', lambda _: "1")
assert set_level() == 3
[...]
```

In the second test function *test_create_solution* I don't know how to test if return of *create_solution* equals certain values, because the values are randomly generated. Therefore I check whether the length of the returned list is equal to the level passed as an argument, and whether all entries in this list are of type integer. I also check if ValueErrors are caught if the level argument is not 3, 4 or 5 (i.e. difficulty + 2).
```
[...]
assert len(create_solution(3)) == 3
assert all(isinstance(n, int) for n in create_solution(3)) == True
[...]
```

The third function *test_get_input* again simulates user input and checks whether codebreaker's input is correctly turned into a list which length equals the difficulty level and whether all entries in this list are of type integer. I also check if ValueErrors are catched if the input contains something else than digits or is not of correct length. **In order to raise these ValueErrors, for testing I need to uncomment the respective line of code in *get_input* and comment the 'pass' line.**
```
[...]
monkeypatch.setattr('builtins.input', lambda _: "123")
assert get_input(3) == [1, 2, 3]
[...]
```

In the last function *test_check_guess* I finally check if matching solution and guess return False (in order to break the *main* while loop), if a count of 10 returns False, and if non-matching guesses reurn True (in order to stay in the *main* while loop).
```
[...]
assert check_guess([1, 2, 3], 3, 1, [1, 2, 3]) == False
assert check_guess([1, 2, 3], 3, 10, [0, 2, 3]) == False
assert check_guess([1, 2, 3], 3, 1, [0, 2, 3]) == True
[...]
```

With this minimal set of unit tests in place, I could add a lot of variances in order to test different input and returns.